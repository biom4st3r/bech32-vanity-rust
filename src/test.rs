use std::time;
#[allow(dead_code)]
fn run() {
    let mut foo = String::from("qpwoeirutyalskdjfhgzmxncbvQPWOEIRUTYALSKDJFHGZMXNCBVqpwoeirutyalskdjfhgzmxncbvQPWOEIRUTYALSKDJFHGZMXNCBV");
    let mut collect_time: u128 = 0;
    let mut inplace_time: u128 = 0;
    for _x in 0..10000 {
        let mut start = time::Instant::now();
        collect(&mut foo);
        collect_time += start.elapsed().as_nanos();
        start = time::Instant::now();
        inplace(&mut foo);
        inplace_time += start.elapsed().as_nanos();
    }
    println!("collect takes ~{} nanos | total: {}", collect_time as f64 / 10000f64, collect_time);
    println!("inplace takes ~{} nanos | total: {}", inplace_time as f64 / 10000f64, inplace_time);
}

fn collect(array: &mut String) -> String {
    array.chars().rev().collect()
}

fn inplace(array: &mut String) {
    let mut index = 0;
    for _x in (0..array.len()).step_by(2) {
        let char = array.pop().unwrap();
        array.insert(index, char);
        index+=1;
    }
}
