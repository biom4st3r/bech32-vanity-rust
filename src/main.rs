use con_win::Window;
use base58::ToBase58;
use cashweb_secp256k1::{Secp256k1};
use cashweb_secp256k1::key;

use rand::{rngs::OsRng};
use rand::{self, RngCore};


use ring::digest;
use ripemd160::{Ripemd160, Digest};
use generic_array::GenericArray;
use generic_array::typenum::{*};
use std::{env, io::Write, sync::Mutex, thread, time::{self, Duration}};
use regex::Regex;
use std::fmt::{Display, Result, Formatter};
use std::sync::Arc;
use std::sync::atomic::{AtomicU64,Ordering};
use std::fs::File;

mod test;
mod con_win;

const BECH: [char ; 32] = [
    'q', 'p', 'z', 'r', 'y', '9',
    'x', '8', 'g', 'f', '2', 't', 
    'v', 'd', 'w', '0', 's', '3', 
    'j', 'n', '5', '4', 'k', 'h', 
    'c', 'e', '6', 'm', 'u', 'a', 
    '7', 'l'
];

pub struct KeyPair {
    pub addr: String,
    pub private: [u8; 32],
}

impl Display for KeyPair {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "Addr: {} Wip: {:?} Secret: {:?} ", self.addr, encode_wip(&self.private), self.private)
    }
}
impl Default for KeyPair {
    fn default() -> KeyPair {
        Self {addr: String::from(""), private: [0;32]}
    }
}

fn main_threaded(threads: u32, regex: String) {
    let counter = Arc::new(AtomicU64::new(0));
    let success = Arc::new(AtomicU64::new(0));
    let start = time::Instant::now();
    let founds = Arc::new(
        Mutex::new(
            Window::new(
                0, 0, 40, 
                vec!["".to_string(); 14])));
    for _x in 0..threads {
        let counter = Arc::clone(&counter);
        let success = Arc::clone(&success);
        let regex = String::clone(&regex);
        let founds = Arc::clone(&founds);
        thread::Builder::new().name(format!("{}",_x)).spawn(move || {
            let re = Regex::new(regex.as_str()).unwrap();
            let secp = &Secp256k1::new(); 
            let key: &mut KeyPair = &mut Default::default();
            loop {
                main_loop(secp, key);
                if re.is_match(key.addr.as_str()) {
                    success.fetch_add(1, Ordering::AcqRel);
                    if let Ok(mut file) = File::create(format!("{}.key", &key.addr)) {
                        match file.write_all(&key.to_string().as_bytes()) {
                            Ok(_val) => (),
                            Err(val) => {
                                eprintln!("Failed to write key to disk. \n{}", val);
                            },
                        }
                    } else {println!("Failed to open file.")}

                    if let Ok(mut val) = founds.as_ref().lock() {
                        val.add(&format!("{}:{}",thread::current().name().unwrap_or("default"), key.addr));
                        
                    };

                    // print!("\x1B[{};{}H{}\n", 0,0, key);
                }
                counter.as_ref().fetch_add(1, Ordering::Relaxed);
            }
        }).unwrap();
    }
    // let ff = vec!["asdf"];

    let mut window = Window::new(45,  5,  25,  vec!["".to_string(),"".to_string(),"".to_string(),"".to_string(),"".to_string()]);
    loop {
        // let old = counter.load(Ordering::SeqCst);
        thread::sleep(Duration::new(0, 12_500_000));
        let new = counter.load(Ordering::SeqCst);
        // let instant = (new-old) as f32 / 0.05f32;
        let success = success.load(Ordering::SeqCst);
        let elapsed = start.elapsed().as_secs_f32();
        let persec = new as f32 / elapsed;
        window.content[0] = format!("Duration: {:.2}secs", elapsed);
        window.content[1] = format!("{} Addrs", new);
        window.content[2] = format!("{:.2} KiAddr/sec", persec / 1000f32);
        window.content[3] = format!("{:.2} Addr/sec/thread", persec / threads as f32);
        window.content[4] = format!("Success Rate: {:.8}%", (success as f32 / new as f32) * 100f32);
        window.draw();
        match founds.as_ref().lock() {
            Ok(val) => val.draw(),
            Err(_) => ()
        };
        print!("\x1B[{};{}H",0, 45)
        // println!("\x1B[1A{} addresses checked in {:.2} secs | {:.2} KiAddr/sec | {:.2} Addr/sec per thread | Success Rate {:.8}%\x1B[K", new, elapsed, persec / 1000f32, persec / threads as f32, (success as f32 / new as f32) * 100f32);
    }
}

fn encode_wip(array: &[u8;32]) -> String {
    let mut wip: Vec<u8> = Vec::with_capacity(32+2+4);
    wip.push(0x80);
    wip.extend(array.iter());
    wip.push(0x01);
    let checksum = digest::digest(&digest::SHA256, digest::digest(&digest::SHA256, wip.as_slice()).as_ref());
    let checksum = checksum.as_ref()[0..4].iter();
    wip.extend(checksum);
    wip.to_base58()
}

#[allow(dead_code)]
fn testing () {

    let mut elp: u128 = 0;
    for _x in 0..10000 {
        let mut sec: GenericArray<u8,U20> = GenericArray::from([52, 34, 81, 53, 128, 76, 244, 85, 116, 81, 3, 240, 246, 5, 11, 181, 19, 53, 34, 101]);
        let start = time::Instant::now();
        big_num_to_bech_no_cache_curr_rev(&mut sec);
        elp += start.elapsed().as_nanos();
    }
    println!("no_cache_curr_rev: cost per {} ns | total {}", elp as f64 / 10_000.0, elp);
    
    let mut elp: u128 = 0;
    for _x in 0..10000 {
        let mut sec: GenericArray<u8,U20> = GenericArray::from([52, 34, 81, 53, 128, 76, 244, 85, 116, 81, 3, 240, 246, 5, 11, 181, 19, 53, 34, 101]);
        let start = time::Instant::now();
        big_num_to_bech_cache(&mut sec);
        elp += start.elapsed().as_nanos();
    }
    println!("cache: cost per {} ns | total {}", elp as f64 / 10_000.0, elp);

    let mut elp: u128 = 0;
    for _x in 0..10000 {
        let mut sec: GenericArray<u8,U20> = GenericArray::from([52, 34, 81, 53, 128, 76, 244, 85, 116, 81, 3, 240, 246, 5, 11, 181, 19, 53, 34, 101]);
        let start = time::Instant::now();
        big_num_to_bech_no_cache(&mut sec);
        elp += start.elapsed().as_nanos();
    }
    println!("no_cache: cost per {} ns | total {}", elp as f64 / 10_000.0, elp);

    let mut elp: u128 = 0;
    for _x in 0..10000 {
        let mut sec: GenericArray<u8,U20> = GenericArray::from([52, 34, 81, 53, 128, 76, 244, 85, 116, 81, 3, 240, 246, 5, 11, 181, 19, 53, 34, 101]);
        let start = time::Instant::now();
        big_num_to_bech_no_cache_curr(&mut sec);
        elp += start.elapsed().as_nanos();
    }
    println!("no_cache_curr: cost per {} ns | total {}", elp as f64 / 10_000.0, elp);

}

fn main() {
    // testing()
    loop {
        let args: Vec<String> = env::args().collect();
        if args.len() < 3 {
            break;
        }

        match args[1].parse::<u32>() {
            Err(_e) => break,
            Ok(val) => main_threaded(val, String::clone(&args[2])),
        };

        std::process::exit(-1);
    }
    println!("Proper Sytanx ./sandbox (threads: u32) (regex: str)");
    println!("Example: {} {} {}", "./sandbox", 8, r"^(\b8l0m4st3r|\bm4st3r|\bcur[7t][lj]s|[0-9]{14,}|0{6,}|1{6,}|2{6,}|3{6,}|4{6,}|5{6,}|6{6,}|7{6,}|8{6,}|9{6,}|\ballys0n)")

}

fn main_loop(secp: &Secp256k1<cashweb_secp256k1::All>, key: &mut KeyPair) {
    
    OsRng.fill_bytes(&mut key.private);
    // let private = key::SecretKey::from_slice(&key.private);
    if let Ok(private) = key::SecretKey::from_slice(&key.private) {
        let public_bytes = key::PublicKey::from_secret_key(secp, &private).serialize();

        let sha_hash = digest::digest(&digest::SHA256, &public_bytes);
        let sha_bytes = sha_hash.as_ref();
        
        let mut ripe: Ripemd160 = Ripemd160::new();
        ripe.update(sha_bytes);
        
        key.addr = big_num_to_bech_no_cache_curr_rev(&mut ripe.finalize());
    }
    else {
        println!("Invalid Private key or something: {}", key)
    }
}

fn array_empty(data: GenericArray< u8, U20 >) -> bool {
    let mut i = 0;
    for x in 0..data.len() {
        i |= data[x];
        if i != 0 {
            return false;
        }
    }
    true
}

fn big_num_to_bech_cache(data: &mut GenericArray< u8, U20 >) -> String {
    let mut out = String::with_capacity(32);
    while !array_empty(*data) {
        out.insert(0, BECH[ (data[19] & 0b1_1111) as usize ]);
        for i in (0..20).rev() {
            let curr = &mut data[i];
            let leading = (*curr & 0b1_1111) << 3;
            // data[i] >>= 5;
            *curr >>= 5;
            if i != 19 {
                data[i+1] |= leading
            }
        }
    }
    out
}

fn big_num_to_bech_no_cache_curr(data: &mut GenericArray< u8, U20 >) -> String {
    let mut out = String::with_capacity(32);
    while !array_empty(*data) {
        out.insert(0, BECH[ (data[19] & 0b1_1111) as usize ]);
        for i in (0..20).rev() {
            let leading = (data[i] & 0b1_1111) << 3;
            data[i] >>= 5;
            if i != 19 {
                data[i+1] |= leading
            }
        }
    }
    out
}

fn big_num_to_bech_no_cache_curr_rev(data: &mut GenericArray< u8, U20 >) -> String {
    let mut out = String::with_capacity(32);
    while !array_empty(*data) {
        out.push(BECH[ (data[19] & 0b1_1111) as usize ]);
        for i in (0..20usize).rev() {
            let leading = (data[i] & 0b1_1111) << 3;
            data[i] >>= 5;
            if i != 19 {
                data[i+1] |= leading
            }
        }
    }
    out.chars().rev().collect()
}

fn big_num_to_bech_no_cache(data: &mut GenericArray< u8, U20 >) -> String {
    let mut out = String::with_capacity(32);
    while !array_empty(*data) {
        out.insert(0, BECH[ (data[19] & 0b1_1111) as usize ]);
        for i in (0..20).rev() {
            if i != 19 {
                data[i+1] |= (data[i] & 0b1_1111) << 3
            }
            data[i] >>= 5;
        }
    }
    out
}
