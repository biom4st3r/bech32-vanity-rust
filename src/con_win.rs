use std::{cmp::min, iter::FromIterator};




pub struct Window {
    x_pos: u32,
    y_pos: u32,
    pub width: u8,
    pub content: Vec<String>,
    index: usize,
}
impl Window {
    pub fn new(x_pos: u32, y_pos: u32, width: u8, content: Vec<String>) -> Window {
        Window {
            x_pos, y_pos, width, content, index: 0
        }
    }

    pub fn draw(&self) {
        let mut output = String::from("");
        for (i,item) in self.content.iter().enumerate() {
            output.push_str( format!("\x1B[{};{}H{}", self.y_pos + i as u32, self.x_pos, String::from_iter(item[..min(self.width as usize, item.len()) ].chars())).as_str());
        }
        print!("{}",output);
    }

    pub fn add(&mut self, val: &String) {
        if self.index >= self.content.len() {
            self.index = 0;
        }
        self.content[self.index] = String::clone(val);
        self.index += 1;
    }


}