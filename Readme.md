# bech32 vanity address generator in 🦀
* Tested on Ryzen 7 3800x
 - 1 core: ~37.5k per core
 - 4 core: ~35.6k per core
 - 7 core: ~31k per core
 - 8 core: ~28k per core
## Usage
* ./sandbox (threads: u32) (regex: str)
* ./sandbox 7 "^(\b8[lj]0m4st3r)"