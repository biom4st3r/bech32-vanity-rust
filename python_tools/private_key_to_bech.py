#!/usr/bin/python3
import base58
import sys
from cryptography.hazmat.primitives.asymmetric import ec
import hashlib
from cryptography.hazmat.primitives import hashes
import bech32

sha256 = hashes.SHA256()


def checksum(data):
    for x in range(2):
        h = hashes.Hash(sha256)
        h.update(data)
        data = h.finalize()
    return data[:4]


def gen_wip_private_key(private_seed: bytes):
    preEncode = bytearray(b'\x80')
    preEncode.extend(private_seed)
    preEncode.extend(b'\x01')
    preEncode.extend(checksum(bytes(preEncode)))
    preEncode = bytes(preEncode)
    return base58.b58encode(preEncode)


def gen_bech_public_key(publicNumbers: ec.EllipticCurvePublicNumbers) -> str:
    pub = ((b'\x02' if publicNumbers.y % 2 == 0 else b'\x03') + publicNumbers.x.to_bytes(32, 'big'))

    sha = hashes.Hash(sha256)
    sha.update(pub)
    sha = sha.finalize()

    h = hashlib.new('ripemd160')
    h.update(sha)
    h = h.digest()
    return bech32.encode('bc', 0, h)

try:
    action = sys.argv[1]
    input = sys.argv[2]
except:
    print('Usage: private_key_to_bech [wip|secret|valid] [WipEncodedPrivateKey|32 length 8-bit array|publicKeyStringToRegexSearch]')



def wip():
    secret: bytes = base58.b58decode(input)
    secret = secret[1:-5]
    sec = []
    for x in secret:
        sec.append(x)
    print(sec)
    privateKey: ec.EllipticCurvePrivateKey = ec.derive_private_key(int.from_bytes(secret, 'big', signed=False), ec.SECP256K1())
    print(gen_bech_public_key(privateKey.public_key().public_numbers()))


def secret():
    sec = []
    for x in input[1:-1].split(', '):
        sec.append(int(x))
    privateKey: ec.EllipticCurvePrivateKey = ec.derive_private_key(int.from_bytes(sec, 'big', signed=False), ec.SECP256K1())
    print(gen_bech_public_key(privateKey.public_key().public_numbers()))
    print(gen_wip_private_key(sec))


def valid():
    BECH = ['q', 'p', 'z', 'r', 'y', '9', 'x', '8', 'g', 'f', '2', 't', 'v', 'd', 'w', '0', 's', '3', 'j', 'n', '5', '4', 'k', 'h', 'c', 'e', '6', 'm', 'u', 'a', '7', 'l']
    ALT = {
        "i": ['l', 'j'],
        "b": ['8'],
        "o": ['0'],
        "1": ['l']
    }
    output = ''
    for x in input:
        if x not in BECH:
            print("%s is invalid" % x)
            if x in ALT:
                if len(ALT[x]) > 1:
                    t = ''
                    for y in ALT[x]:
                        t += y
                    output += '[' + t + ']'
                else:
                    output += ALT[x][0] 
                print("alternatives  %s" % ALT[x])
            else:
                print("No alternative")
                output += x
        else:
            output += x
    print(output)



actions = {
    'wip': wip,
    'secret': secret,
    'valid': valid
}
try:
    actions[action]()
except:
    print('Usage: private_key_to_bech [wip|secret|valid] [WipEncodedPrivateKey|32 length 8-bit array|publicKeyStringToRegexSearch]')
