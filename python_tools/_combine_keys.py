#!/usr/bin/python3
import os

list = []

for x in os.listdir():
    if x.endswith('.key'):
        with open(x,'r') as file:
            list.append(file.read())
        os.remove(x)

with open('keys.keys','a+') as file:
    file.write('\n'.join(list))
